import React, { Fragment } from 'react';
import MarketingApp from './components/MarketingApp.jsx';

export default () => (
	<Fragment>
		<MarketingApp />
	</Fragment>
);
